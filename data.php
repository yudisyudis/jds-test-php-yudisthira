<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'fungsi.php';

// membuat fungsi waktu berlaku dari token 
$token_time = date("d-m-y");
$expired_at = date("d-m-y", strtotime("tomorrow"));
     
       
if ($token_time > $expired_at) {
    unset($_SESSION["token"]);
    session_destroy();
    header('Location: login.php');
    echo "<script>
            alert('Token Expired');
          </script>";
}

// validasi token yang disubmit dengan yang berada di database
if (isset($_POST['validasi'])) {
    if (hash_equals($_SESSION["token"], $_POST["token"])) {
        $is_valid = true;
        $_SESSION['is_valid'] = $is_valid;
        $_SESSION['expired_at'] = $expired_at;

        echo "<script>
                 alert('Validasi Berhasil!!');
              </script>";

        header('Location: valid.php');
    } else
        echo $_SESSION['is_valid'] = false;
        echo "<script>
                 alert('Validasi Gagal!, silahkan hubungi admin@jds.com');
              </script>";
}

if (isset($_POST['logout'])) {
    session_destroy();
    header('Location: login.php');
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JDS Tes | Yudisthira</title>

    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- {{-- Bootsrap CDN --}} -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- {{-- CSS --}} -->
    <link rel="stylesheet" href="/css/style.css">

</head>
<body style="background-image: url(https://source.unsplash.com/1300x700?office)">
<div class="container mt-4 md-5">
    <div class="row justify-content-center m-auto" >
        <div class="col-md-5 justify-content-center card p-5 mb-4" style="background-color: rgba(255, 255, 255, 0.9); border-radius: 20px;">
        
            <h2 class="text-center">Selamat Datang <?php echo $_SESSION['username']; ?></h2>
            <label for="" style="margin-top: 10px">Id</label>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo $_SESSION["id"];
                    ?>
                </div>
            </div>
            <label for="" style="margin-top: 10px">Username</label>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo $_SESSION["username"];
                    ?>
                </div>
            </div>
            <label for="" style="margin-top: 10px">Token</label>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo $_SESSION["token"];
                    ?>
                    
                </div>
            </div>
            <form action="" method="post">
               <input type="hidden" name="token" value="<?php echo $_SESSION["token"]; ?>">
               <button class="w-100 btn btn-lg btn-primary mt-4" name="validasi" type="submit">Validasi</button>
            </form>

            <form action="" method="post" class="mx-auto">
                <button class="w-100  btn btn-md btn-danger mt-4" name="logout" type="submit">Logout</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>