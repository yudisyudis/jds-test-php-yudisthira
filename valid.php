<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

echo "<script>
        alert('Validasi Berhasil!!');
    </script>";

require 'fungsi.php';

if (isset($_POST['logout'])) {
    session_destroy();
    header('Location: login.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JDS Tes | Yudisthira</title>

    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- {{-- Bootsrap CDN --}} -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- {{-- CSS --}} -->
    <link rel="stylesheet" href="/css/style.css">

</head>
<body style="background-image: url(https://source.unsplash.com/1300x700?office)">
<div class="container mt-4 md-5">
    <div class="row justify-content-center m-auto" >
        <div class="col-md-5 justify-content-center card p-5" style="background-color: rgba(255, 255, 255, 0.9); border-radius: 20px;">
        
            <h2 class="text-center"><strong>Validasi Berhasil!</strong></h2>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo "Status Validasi: ";
                        echo $_SESSION["is_valid"];
                    ?>
                </div>
            </div>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo "Berlaku hingga: ";
                        echo $_SESSION["expired_at"];
                    ?>
                </div>
            </div>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo "Pengguna: ";
                        echo $_SESSION["username"];
                    ?>
                    
                </div>
            </div>
            <small class="d-block text-center mt-3"><a href="data.php">Kembali</a></small>
            <form action="" method="post" class="mx-auto">
                <button class="w-100  btn btn-md btn-danger mt-4" name="logout" type="submit">Logout</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>