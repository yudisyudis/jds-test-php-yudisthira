-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: jds_test
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Super Admin','Admin','User') COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'yudisyudis','Super Admin','dxyKuG',NULL,'2022-06-21 20:09:12','2022-06-25 10:45:27','nVGl3LjKo1A0lMgmmR86w3Dr4t6OcsVTWGtMe2ch'),(4,'yudis88','User','CkxpuT',NULL,'2022-06-22 15:10:38','2022-06-24 23:24:31','uOglI6BRySorjIu3xmzdNuT0uowukfZ3qq2JtSBn'),(5,'yudis2','Admin','9ADovZ',NULL,'2022-06-23 02:09:54','2022-06-23 02:19:34','qPgtzLjIqZzoMeeVAzyYl5JPieyCQVVyHu7j53t8'),(7,'yudis3','Admin','o1SFb4',NULL,'2022-06-23 02:12:06','2022-06-23 02:12:06',NULL),(9,'yudis4','Admin','bUEm0T',NULL,'2022-06-23 02:13:25','2022-06-23 02:13:25',NULL),(10,'yudis5','Admin','4YqeVJ',NULL,'2022-06-23 02:14:10','2022-06-23 02:14:10',NULL),(11,'yudis5a','Admin','qZMDNm',NULL,'2022-06-23 02:15:36','2022-06-23 02:15:36',NULL),(12,'yudis5afe','Admin','dlxQbJ',NULL,'2022-06-23 02:16:28','2022-06-23 02:16:28',NULL),(14,'yudis96','User','YvjlTp',NULL,'2022-06-24 23:13:49','2022-06-24 23:13:49',NULL),(16,'yudis67','Admin','91NA3B',NULL,'2022-06-24 23:14:49','2022-06-24 23:16:19','Fa5cPKPTNvsFChxhXWv94Ss06mk3arJ8X9wwh7dR'),(17,'adis','Super Admin','kTg3Ro',NULL,NULL,NULL,NULL),(18,'cekhasil','Admin','WHm2Aa',NULL,NULL,NULL,NULL),(19,'dfdf','Super Admin','5UND0o',NULL,NULL,NULL,NULL),(20,'hjktyjyujtj','Admin','A8otrx',NULL,NULL,NULL,NULL),(21,'hjktyjyujtjrfe','Admin','OtVbJo',NULL,NULL,NULL,NULL),(22,'hghg','User','jdlDP7',NULL,NULL,NULL,NULL),(23,'gmgh','Admin','SCjfX7',NULL,NULL,NULL,NULL),(24,'jtjrt','User','ZQKuE2',NULL,NULL,NULL,NULL),(25,'ahmad','Admin','nJSMq2',NULL,NULL,NULL,NULL),(26,'barjo','Admin','XosMgR',NULL,NULL,NULL,NULL),(27,'jubaedah','Super Admin','$2y$10$EOy3LpJTbS01Vp.rO8Wm7OufTboArhB465dzKGrmboYTX0JboloS6',NULL,NULL,NULL,NULL),(28,'zainudin','Super Admin','$2y$10$gnuQ/LI.qdS6xPtOeUmZqeO4/NilIhImH5CFiC6bAHiXfzGrI.DlG',NULL,NULL,NULL,NULL),(29,'jidan','Super Admin','$2y$10$RoVBi1HM7tAxlF6jXzf5ce3ezSt4o9NiWlEI5bcoPjWztGQd.zRsq',NULL,NULL,NULL,'8d561edaf2891a7d2069ded75c73ea735af1705c5d2992b6fc5d279c3762ae5b'),(30,'','Super Admin',NULL,NULL,NULL,NULL,'e3fdc239e11fb811be813416665ae429035ca393618c22257359bb9a670dd374'),(31,'piko','Super Admin','$2y$10$dCmJqUJQKA6JF3DWfobuLutE8XWKd7x2f5/Tmk2WTHeIriaJcbYMa',NULL,NULL,NULL,NULL),(32,'kiko','Super Admin','$2y$10$ceO0MK.3k.1qy/pQ/fGO9.j1Jmzp8W9SWudm9uMPpgXtnuUxdxdwe',NULL,NULL,NULL,'49d09471d38d76de672f4bfab5d43669c370e66bf713e6d69e2a78269c7c3e71');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-28  9:19:47
