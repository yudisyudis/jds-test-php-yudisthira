Jabar Digital Service Screening Tes - Backend Developer

I. Pendahuluan

Aplikasi Backend ini dibuat dengan berdasarkan pada persyaratan yang dibuat yaitu:

1. Terdapat endpoint registrasi yang menerima input username dan role serta mengeluarkan output username, role, dan password, dimana passwordnya digenerate secara otomatis sehingga terdiri dari 6 karakter yang tersusun atas kombinasi huruf dan angka

2. Terdapat endpoint login yang menerima input username dan password serta mengeluarkan output id, username, dan token.

3. Terdapat endpoint untuk melakukan validasi token yang mengeluarkan output parameter is_valid, expired_At, dan username


Dalam membuat aplikasi ini, digunakan bahasa pemrograman PHP 8 dengan mengaplikasikan 3 komponen utama pembuatan web yaitu HTML, CSS, Javascript. Serta menggunakan bootstrap untuk merapihkan tampilan aplikasi. Database yang digunakan pada local server adalah MySql. Dimanfaatkan pula API unsplash.com untuk mengenerate gambar secara random sesuai dengan kategori yang ditentukan.

II. Cara kerja aplikasi

1. registrasi.PHP

pada halaman registrasi.php akan ditampilkan sebuah form untuk melakukan registrasi dimana user diminta untuk mengisi username dan role yang tersedia. Tiap-tiap form isian bersifat required yang berarti tidak diperkenankan ada form yang tidak diisi. 

Setelah form diisi maka user dapat melakukan submit untuk kemudian halaman akan berpindah ke halaman info.php dimana user memperoleh password yang tergenerate otomatis. 

Selain itu, pada halaman registrasi.php terdapat pula link yang menghubungkan ke halaman login, untuk memfasilitasi user yang telah melakukan registrasi sebelumnya.

proses backend registrasi.info:

setelah user melakukan submit form, maka logic untuk menjalankan fungsi akan aktif. Pada konteks registrasi.php tombol submit akan menjalankan fungsi registrasi yang terdapat pada halaman fungsi.php serta mengaktifkan session registrasi yang berguna untuk menyimpan nilai inputan pada form pada variabel $_SESSION.

fungsi registrasi pada halaman fungsi.php berguna untuk mengenerate password secara otomatis sesuai ketentuan yaitu 6 karakter kombinasi huruf dan angka. Kemudian pada fungsi registrasi pula seluruh nilai ouput dari fitur registrasi dideklarasikan agar dapat ditampilkan di halaman lain dan di insert ke dalam database. Fungsi registrasi diakhiri dengan melakukan redirect ke halaman info.php.


2. info.php

info.php merupakan halaman yang dapat diakses setelah user melakukan submit form registrasi. Pada halaman ini akan ditampilkan data registrasi user yang terdiri dari username, role, dan password yang tergenerate otomatis. Kemudian di bagian bawah info data registrasi user terdapat link yang mempersilahkan user melakukan login ke halaman login.php.

proses backend info.php:

penampilan nilai nilai yang berasal dari form registrasi pada halaman info.php menggunakan variabel global $_SESSION yang telah dideklarasikan sebelumnya di halaman fungsi.php.

Halaman info hanya dapat diakses ketika variabel $_SESSION["register"] bernilai true, paramater ini sendiri akan otomatis aktif ketika user telah melakukan proses registrasi yang benar.

3. login.php

pada halaman login.php terdapat form login yang menerima dua nilai input yaitu username dan password, serta disisipka link yang mengarahkan user ke halaman registrasi untuk memfasilitasi user yang belum melakukan registrasi. 

setelah user berhasil melakukan submit form login, maka user akan diarahkan ke halaman data.php yang akan menampilkan informasi login user berupa Id, Username, dan token yang tergenerate secara otomatis.

proses backend login.php:

setelah submit form login dilakukan maka akan mengaktifkan fungsi verifikasi yang terdiri dari beberapa tahap yaitu:
1. mendeklarasikan variabel yang berisikan nilai input form
2. mengambil data dari database berdasarkan username yang diperoleh dari input form
3. mengecek apakah password yang berasal dari input form sesuai dengan password yang diambil dari database

jika verifikasi berhasil maka fungsi login pada halaman fungsi.php akan dijalankan

fungsi login pada fungsi.php berguna untuk mengenerate token yang akan digunakan untuk validasi user, kemudian terdapat pula perintah untuk mengupdate database dengan menginsert data tambahan berupa token pada tabel users yang usernamenya sesuai dengan username data yang input form.

kemudian pada fungsi login terdapat pula deklarasi variable yang berisikan nilai nilai yang akan ditampilkan sebagai output dari fitur login, menggunakan global variabel $_SESSION.

fungsi login diakhiri dengan meredirect ke halaman data.php yang kemudian akan menampilkan nilai output dari form login.

4. data.php

halaman data.php menampilkan informasi user seperti id, username, dan token yang berlaku. Pada halaman ini terdapat pula tombol logout yang berfungsi untuk menghentikan sesi dan menghapus token yang berlaku dan mengarahkan kembali user ke halaman login.php. Kemudian pada halaman data.php terdapat pula tombol validasi yang berfungsi untuk memvalidasi token apakah masih berlaku atau sudah expired.

proses backend data.php:

pada halaman data.php terdapat fungsi untuk mengatur lamanya token berlaku, dimana pada fungsi tersebut diatur bahwa token memiliki waktu aktif 1 hari sejak digenerate otomatis. Jika token telah aktif lebih dari 1 hari maka fungsi untuk menonaktifkan token akan berlangsung sekaligus mengakhiri sesi yang berlaku.

kemudian tombol validasi pada halaman data.php jika disubmit maka akan menjalankan perintah validasi, dengan membandingkan token yang terdapat di database dengan token yang terinput secara hidden melalui form validasi. Jika token sesuai, maka akan dideklarasikan variabel-variabel validasi yaitu is_valid, expired_at, dan username menggunakan variabel global $_SESSION. Submit validasi diakhri dengan mengarahkan user ke halaman valid.php sementara tombol lainnya yaitu Logout berfungsi untuk mengakhiri sesi dan mengarahkan kembali user ke halaman login.php.

Halaman data.php hanya dapat diakses ketika variabel $_SESSION["login"] bernilai true, paramater ini sendiri akan otomatis aktif ketika user telah melakukan proses login yang benar.

5. valid.php

halaman valid.php menampilkan status validasi token yang terdiri dari tiga nilai yaitu apakah token valid atau tidak, dengan nilai 1 menandakan valid, dan nilai 0 menandakan tidak valid. kemudian yang kedua adalah waktu expired token yang disetting 1 hari setelah token digenerate, serta yang ketiga adalah username pemilik token.

setelah token tervalidasi, user dapat kembali ke halaman data.php atau langsung logout untuk mengakhiri sesi.

proses backend valid.php:

output nilai yang ditampilkan di halaman valid.php berasal dari nilai yang dihasilkan di halaman login.php dengan memanfaatkan global variabel $_SESSION.

kemudian terdapat pula tombol login yang berfungsi untuk mengakhiri sesi dan mengarahkan user ke halaman login.php ketika digunakan.

Sama seperti halaman data.php, halaman valid.php hanya dapat diakses ketika user telah melakukan proses login dengan benar yang mengaktifkan variabel $_SESSION["login"]
