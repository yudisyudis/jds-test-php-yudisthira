<?php

session_start();

if (!isset($_SESSION["register"])) {
    header("Location: registrasi.php");
    exit;
}

require 'fungsi.php';

echo "<script>
        alert('registrasi berhasil');
      </script>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JDS Tes | Yudisthira</title>

    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- {{-- Bootsrap CDN --}} -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- {{-- CSS --}} -->
    <link rel="stylesheet" href="/css/style.css">

</head>
<body style="background-image: url(https://source.unsplash.com/1300x700?office)">
<div class="container mt-4 md-5">
    <div class="row justify-content-center m-auto" >
        <div class="col-md-5 justify-content-center card p-5" style="background-color: rgba(255, 255, 255, 0.9); border-radius: 20px;">
        
            <h2 class="text-center">Info Registrasi User</h2>
            <label for="" style="margin-top: 10px">Username</label>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo $_SESSION["username"];
                    ?>
                </div>
            </div>
            <label for="" style="margin-top: 10px">Role</label>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo $_SESSION["role"];
                    ?>
                </div>
            </div>
            <label for="" style="margin-top: 10px">Password</label>
            <div class="card">
                <div class="card-body md-4">
                    <?php 
                        echo $_SESSION["password"];
                    ?>
                    
                </div>
            </div>
            <small class="d-block text-center mt-3 bg-light">Silahkan <a href="login.php">Login</a></small>
        </div>
    </div>
</div>
</body>
</html>