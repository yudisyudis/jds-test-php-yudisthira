<?php
// Mulai Sesi
session_start();

// menghubungkan dengan fungsi
require 'fungsi.php';

// menjalankan serangkaian perintah setelah dilakukan submit
if (isset($_POST['login'])) {
    $_SESSION["login"] = true;


    // declare variabel username, password dan hasil
    $username = $_POST['username'];
    $password = $_POST["password"];

    $hasil = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");

    // verifikasi password yang disubmit dengan yang terdapat di database, sebagai syarat menjalankan fungsi login
    if (mysqli_num_rows($hasil) === 1) {
       $row = mysqli_fetch_assoc($hasil);
       if (password_verify($password, $row['password'])){
            if (login($_POST) > 0) {
                echo "<script>
                        alert('login berhasil');
                    </script>";
            }else{
                echo mysqli_error($conn);
            }
            
       };
    }
    $error = true;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JDS Test | Yudisthira</title>

    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- {{-- Bootsrap CDN --}} -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- {{-- CSS --}} -->
    <link rel="stylesheet" href="/css/style.css">

    <?php if (isset($error)) : ?>
        <p style="color: red; font-style: italic;">Username / Password salah!</p>
    <?php endif; ?>

    <style>
        label{
            display: block;
        }

        ul{
            list-style-type: none;
        }

        li{
            margin-bottom: 25px;
        }
    </style>
</head>
<body style="background-image: url(https://source.unsplash.com/1300x700?office)">
<div class="container mt-4 md-5">

    <div class="row justify-content-center">
        <div class="col-lg-5">

            <main class="form-signin w-100 m-auto card p-5" style="background-color: rgba(255, 255, 255, 0.9); ">
                <h1 class="h3 mb-3 fw-normal text-center">Silahkan Login</h1>
                <form action="" method="post">
            
                <div class="form-floating">
                    <input type="text" name="username" class="form-control"  id="username" placeholder="username" autofocus required>
                    <label for="username">Username</label>
                </div>

                <div class="form-floating">
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                    <label for="password">Password</label>
                </div>
                
                <button class="w-100 btn btn-lg btn-primary mt-3" name="login" type="submit">Login</button>
                </form>

                <small class="d-block text-center mt-3">Belum Register? <a href="registrasi.php">Silahkan registrasi</a></small>
            </main>
        </div>
    </div>

</div>
</body>
</html>