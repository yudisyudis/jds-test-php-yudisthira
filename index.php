<?php

// menghubungkan dengan fungsi
require 'fungsi.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JDS Tes | Yudisthira</title>
    
    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- {{-- Bootsrap CDN --}} -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- {{-- CSS --}} -->
    <link rel="stylesheet" href="/css/style.css">
    
</head>
<body style="background-image: url(https://source.unsplash.com/1300x700?office)">
<!-- form registrasi -->
<div class="container mt-4 md-5">

    <div class="row justify-content-center" >
        <div class="col-lg-5">
            <main class="form-regis w-100 m-auto card p-5" style="background-color: rgba(255, 255, 255, 0.9); ">
                <h1 class="h3 mb-3 fw-normal text-center">Jabar Digital Service</h1>
                <h3 class="h3 mb-3 fw-normal text-center">Screening Test-Backend Dev</h3>
                
                <small class="d-block text-center mt-3" ><strong><a href="registrasi.php" style="text-decoration: none;">Silahkan Registrasi</a></strong></small>
                <small class="d-block text-center mt-3"><strong>Sudah Registrasi? <a href="login.php" style="text-decoration: none;">Silahkan login</a></strong></small>


            </main>
        </div>
    </div>
</div>

</body>
</html>