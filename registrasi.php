<?php
// Mulai Sesi
session_start();

// menghubungkan dengan fungsi
require 'fungsi.php';

// menjalankan fungsi register dalam file fungsi.php setelah submit dilakukan
if (isset($_POST["register"])) {
    $_SESSION["register"] = true;

    if (registrasi($_POST) > 0) {
        echo "<script>
                alert('registrasi berhasil');
             </script>";
    }else{
        echo mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JDS Tes | Yudisthira</title>

    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">

    
    <link rel="icon" href="https://digitalservice.jabarprov.go.id/wp-content/uploads/2019/11/logo_jds.png">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- {{-- Bootsrap CDN --}} -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- {{-- CSS --}} -->
    <link rel="stylesheet" href="/css/style.css">
    
</head>
<body style="background-image: url(https://source.unsplash.com/1300x700?office)">
<!-- form registrasi -->
<div class="container mt-4 md-5">

    <div class="row justify-content-center" >
        <div class="col-lg-5">
            <main class="form-regis w-100 m-auto card p-5" style="background-color: rgba(255, 255, 255, 0.9); ">
                <h1 class="h3 mb-3 fw-normal text-center">Form Registrasi</h1>
                <form action="" method="post">
 
                    <div class="form-floating">
                        <input type="text" name="username" class="form-control rounded-top " id="username" placeholder="Username" required >
                        <label for="username">Username</label>
                    </div>
                    <div class="my-3">
                        <label for=""><strong>Role:</strong></label>
                        
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="super_admin" value="Super Admin">
                            <label class="form-check-label" for="super_admin">
                            Super Admin
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="admin" value="Admin">
                            <label class="form-check-label" for="admin">
                            Admin
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="user" value="User" checked>
                            <label class="form-check-label" for="user">
                            User
                            </label>
                        </div>
                    </div>
                                       
                    <button class="w-100 btn btn-lg btn-primary mt-3" type="submit" name="register">Register</button>
                </form>
                <small class="d-block text-center mt-3">Sudah Register? <a href="login.php">Silahkan login</a></small>


            </main>
        </div>
    </div>
</div>

</body>
</html>