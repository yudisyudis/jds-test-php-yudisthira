<?php

$conn = mysqli_connect("localhost", "root", "", "jds_test");


function registrasi($data)
{
    global $conn; 
    
    // men-generate password yang terdiri dari 6 karakter berisikan huruf dan angka
    $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890');

    // men-declare berbagai variabel yang diperlukan
    $username = strtolower (stripslashes ($data["username"]));
    $role = $data["role"];
    $password = substr($random, 0, 6);
    $password_hash = password_hash($password, PASSWORD_DEFAULT);

    // memastikan username yang diregistrasikan bersifat unik
    $duplicate = mysqli_query($conn, "SELECT username FROM users WHERE username = '$username'");
    if (mysqli_fetch_assoc($duplicate)) {
        echo "<script>
                alert('username sudah terdaftar')
             </script>";
        return false;
    }

    mysqli_query($conn, "INSERT INTO users (username, role, password) VALUES('$username', '$role', '$password_hash')");

    $user = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");
    $user = mysqli_fetch_array($user);

    // mempersiapkan data yang akan ditampilkan sebagai output dari registrasi
    $_SESSION["username"] = $user["username"];
    $_SESSION["role"] = $user["role"];
    $_SESSION["password"] = $password;

    if ($_SESSION["username"] != null) {
        header('Location: info.php');
    }

 
}

function login($data)
{
    global $conn;

    // men-generate token
	if (empty($_SESSION['key']))
		$_SESSION['key'] = bin2hex(random_bytes(32));

	$token = hash_hmac('sha256', 'this is some string: index.php', $_SESSION['key']);

    // penyeragaman format username (huruf kecil seluruhnya)
    $username = strtolower (stripslashes ($data["username"]));

	mysqli_query($conn, "UPDATE users SET _token = '$token' WHERE username = '$username'");


    $user = mysqli_query($conn, "SELECT * FROM users WHERE username = '$username'");
    $user = mysqli_fetch_array($user);

    // mempersiapkan data yang akan ditampilkan sebagai output fitur login
    $_SESSION["id"] = $user["id"];
    $_SESSION["username"] = $user["username"];
    $_SESSION["token"] = $user["_token"];

    if ($_SESSION["username"] != null) {
        echo "<script>
                alert('login berhasil');
            </script>";
        header('Location: data.php');
    }


}

?>